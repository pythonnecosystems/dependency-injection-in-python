# Python에서 종속성 주입 

## 종속성 주입에 대한 간단한 소개
**SOLID** 원칙에 대해 들어본 적이 없나요? 이는 유지 관리가 가능하고 확장 가능한 소프트웨어를 개발하기 위한 객체 지향 소프트웨어 설계의 5가지 원칙이다.

그 원칙 중 하나는 상위 모듈이 하위 모듈에 종속되어서는 안 되고 **추상화(abstraction)**에 종속되어야 한다는 **의존성 반전(dependency inversion)**(DIP) 원칙이다. 이 개념은 종종 **제어의 역전(inversion of control)**(IoC)과 혼동되기도 하지만, **dependency injection**은 IoC를 달성하기 위한 특정 기술이다.

할리우드 원칙이라고도 불리는 제어의 역전에서는 한 개체가 다른 개체를 직접 호출하여 작업을 수행하는 대신 어플리케이션의 흐름을 제어하는 제3자를 통해 두 개체 간에 간접적인 관계가 설정된다. 이 세 번째 엔티티는 일반적으로 종속성 컨테이너로, 종속성을 명시적으로 요청하는 객체가 아니라 종속성을 사용해야 하는 객체에 필요한 종속성을 주입한다.

다음 예를 살펴보자.

![](./1_2L9xNO7RCBzf7uiWxBM1vA.webp)

생성자에서 필수 속성('MarketDataProvider'와 'InstrumentsRepository')을 전달하여 `ExchangeImporter` 클래스의 정의를 만들 수 있다.

```python
class ExchangeImporter:  
    def __init__(self, market_data_provider: MarketDataProvider, instruments_repository: InstrumentsRepository) -> None:  
        self.logger = logging.getLogger(  
            f"{__name__}.{self.__class__.__name__}",  
        )  
        self.market_data_provider = market_data_provider  
        self.instruments_repository = instruments_repository  

    def import_exchange_instruments(self, symbol):  
        ...
```

이 포스팅에서는 종속성을 확인하고 해결하는 데 초점을 맞출 것이므로 이해를 돕기 위해 클래스의 내부 코드는 표시하지 않겠다.

## 종속성 주입 준비
Python에서는 여러 가지 방법으로 종속성 주입을 할 수 있지만, 라이브러리 [dependency injector](https://python-dependency-injector.ets-labs.org/)를 추천한다.

```bash
$ poetry add dependency-injector
```

종속성을 삽입하는 종속성 컨테이너는 **선언적(declarative)**이거나 **동적(dynamic)**일 수 있다. 동적 타입은 어플리케이션의 구조가 어플리케이션이 이미 실행 중일 때만 액세스할 수 있는 파일 또는 기타 소스(데이터베이스, API 등)의 구성에 따라 달라지는 경우에 적합하다.

우리 경우에는 동적 컨테이너가 필요하지 않고 선언적 컨테이너가 필요하며, 다음 파일 `containers.py`에서 만들 것이다.

```python
from dependency_injector import containers, providers  

class Container(containers.DeclarativeContainer):
    pass
```

클래스 내에서 어트리뷰트를 사용하여 공급자를 선언한다. **공급자**는 DI로 객체를 "어셈블"할 수 있게 해주는 역할을 한다.

## 공급자

### 싱글톤(Singleton)과 팩토리 공급자
**싱글톤**은 클래스가 하나의 인스턴스를 갖도록 하는 동시에 해당 인스턴스에 대한 전역 액세스 포인트를 제공할 수 있는 새로운 디자인 패턴이다.

이전 그림의 각 객체에 대해 서로 다른 제공자를 생성한다.

```python
class Container(containers.DeclarativeContainer):

    instruments_repository = providers.Singleton(  
        SqlAlchemyInstrumentsRepository,  
        session_factory=db.provided.session,  
    )  

    market_data_provider = providers.Singleton(  
        EodMarketDataProvider,  
        token='62193c3d043499.xxxxxxxx'  
    )
    exchange_importer = providers.Singleton(  
        ExchangeImporter,  
        market_data_provider=market_data_provider,  
        instruments_repository=instruments_repository)
```

*매번 새 객체를 생성하려는 경우 공급자 **Factory**를 사용할 수 있다.*

*`providers.Singleton`의 첫 번째 매개 변수는 인스턴스화되는 클래스의 이름이다. 마지막 줄은 `ExchangeImporter` 객체를 초기화하는 데 필요한 매개 변수를 보인다.*

종속성이 자동으로 생성되도록 메인 프로그램을 만들려면 다음 코드를 사용한다.

```python
@inject  
def main(            
        exchange_importer: ExchangeImporter = Provide[Container.exchange_importer]  
) -> None:  
    logger = logging.getLogger(__name__)  
    exchange_importer.import_exchange_instruments('GSPC.INDX')  


  if __name__ == "__main__":  
    container = Container()  
    container.init_resources()  
    container.wire(modules=[__name__])  
    main(*sys.argv[1:])
```

데코레이터 **@inject**는 의존성 주입을 수행하는 데코레이터이다. 메인 함수뿐만 아니라 모든 함수에서 사용할 수 있다. 함수 내에서 `Provide[Container.name_object]`` 를 전달하여 주입을 선언한다.

### 구성 공급자 (Configuration provider)
다른 공급자에게 구성 옵션을 제공하기 위해 구성 공급자가 있으며, 이 공급자를 사용하면 `.ini`, `.yaml`, `.json` 파일 또는 환경 변수 등에서 옵션을 로드할 수 있다.

이 튜토리얼에서는 **`yaml`** 옵션을 선택했다.

`containers.py` 파일 내부에서는 `Configuration` 공급자를 사용한다.

```python
from pathlib import Path

class Container(containers.DeclarativeContainer):  
    yaml_file_path = os.path.join(Path(__file__).parent, 'config.yml')  
    config = providers.Configuration(yaml_files=[yaml_file_path], strict=False)  
```

앞서 컨테이너에서 **EodMarketDataProvider**에 대한 고정 값 *토큰*을 정의했지만, **구성** 공급자 덕분에 삽입되는 `config.yaml` 파일의 구성 변수를 사용하는 것이 가장 정확하다.

```python
adapters:  
  eod_api_token: "62193c3d043499.xxxxxxxx"
```

```python
market_data_provider = providers.Singleton(  
    EodMarketDataProvider,  
    token=config.adapters.eod_api_token.required()  
)
```

어플리케이션을 실행하는 환경(로컬, 개발, 프로덕션 등)에 따라 구성 값을 로드하는 것이 일반적이다.

```python
import os

if __name__ == "__main__":
    container = Container()
    container.config.from_yaml("./config.yml")
    if os.environ.get("EXECUTION_ENV") == "local":
        container.config.from_yaml("./config.local.yml")
```

`python-dependency-injector` 라이브러리를 사용하면 여러 파일을 점진적으로 로드할 수도 있다. 위에 표시된 예제에서는 `config.yml`과 환경 변수 `EXECUTION_ENV`에 `local` 값이 있는지 여부에 따라 `config.local.yml` 파일도 로드된다.

### 리소스 공급자
리소스 공급자를 통해 로깅, 풀 등을 초기화하고 구성할 수 있다.

표준 *logging* 라이브러리만으로도 다양한 방식으로 구성을 사용할 수 있지만, 모든 것이 리소스 공급자로 중앙 집중화되어 있다는 점이 흥미롭다. *logging.config.dictConfig* 옵션을 사용하여 yaml 파일에 구성 정의를 전달할 수 있다.

```python
class Container(containers.DeclarativeContainer):
    ...

  logging = providers.Resource(  
        logging.config.dictConfig,  
        config=config.core.logging,  
    )
```

`config.yaml` 파일에 있는 섹션의 구성 예는 다음과 같다.

```yml
core:
  i18n:
    language: 'es'
  logging:
    version: 1
    formatters:
      standard:
        format: "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
      error:
        format: "%(asctime)s - %(name)s - %(levelname)s <PID %(process)d:%(processName)s>\
          \ %(name)s.%(funcName)s(): %(message)s"
    handlers:

      root_file_handler:
        class: logging.handlers.TimedRotatingFileHandler
        level: INFO
        formatter: standard
        filename: /Users/xescuder/Projects/investing-bot/bot/logs/logs.log
        when: midnight
        backupCount: 30
        encoding: utf8

      error_file_handler:
        class: logging.handlers.TimedRotatingFileHandler
        level: ERROR
        formatter: error
        filename: /Users/xescuder/Projects/investing-bot/bot/logs/logs.log
        when: midnight
        backupCount: 20
        encoding: utf8

      screener_file_handler:
        class: logging.handlers.TimedRotatingFileHandler
        level: DEBUG
        formatter: standard
        filename: /Users/xescuder/Projects/investing-bot/bot/logs/screener.log
        when: midnight
        backupCount: 30
        encoding: utf8

      console:
        class: logging.StreamHandler
        level: DEBUG
        formatter: standard
        stream: ext://sys.stdout

    root:
      level: INFO
      handlers: [console, root_file_handler, error_file_handler]

    loggers:
      screener:
        level: DEBUG
        handlers: [console, screener_file_handler]
        propagate: false
```

## 마치며
이 포스팅에서는 실제 예를 통해 Python **Dependency Injector** 라이브러리와 가장 많이 사용되는 공급자를 사용하여 의존성 주입을 구현하는 방법을 살펴보았다.
